import sys

from PIL import Image

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from io import BytesIO

# Create your models here.
User = get_user_model()

PRODUCT_CATEGORIES = (
    ('electronics', 'Electronics'),
    ('for_house', 'For House'),
    ('books', 'Books')
)


def get_models_for_count(*model_names):
    return [models.Count(model_name) for model_name in model_names]


def get_product_url(obj, viewname):
    ct_model = obj.__class__._meta.model_name
    return reverse(viewname, kwargs={'ct_model': ct_model,
                                     'slug': obj.slug})


class MinResolErrorExceprion(Exception):
    pass


class MaxResolErrorExceprion(Exception):
    pass


class LatestProductsManager:

    @staticmethod
    def get_products_for_main_page(*args, **kwargs):
        with_respect_to = kwargs.get('with_respect_to')
        products = []
        ct_models = ContentType.objects.filter(model__in=args)
        for ct_model in ct_models:
            model_products = ct_model.model_class()._base_manager.all().order_by('-id')[:5]
            products.extend(model_products)
        if with_respect_to:
            ct_model = ContentType.objects.filter(model=with_respect_to)
            if ct_model.exists():
                if with_respect_to in args:
                    return sorted(
                        products, key=lambda x: x.__class__._meta.model_name.startswith(with_respect_to),
                        reverse=True
                    )
        return products


class LatestProducts:
    objects = LatestProductsManager()


class CategoryManager(models.Manager):
    CATEGORY_NAME_COUNT_NAME = {
        'Notebook': 'notebook__count',
        'Smartphone': 'smartphone__count'
    }

    def get_queryset(self):
        return super().get_queryset()

    def get_categories_navbar(self):
        models = get_models_for_count('notebook', 'smartphone')
        qs = list(self.get_queryset().annotate(*models))
        data = [
            dict(name=c.name, url=c.get_absolute_url(), count=getattr(c, self.CATEGORY_NAME_COUNT_NAME[c.name]))
            for c in qs
        ]
        return data


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Category')
    slug = models.SlugField(unique=True)
    objects = CategoryManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:category_detail', kwargs={'slug': self.slug})


class Product(models.Model):
    MIN_RESOLUTION = (400, 400)
    MAX_RESOLUTION = (2560, 1080)
    MAX_IMAGE_SIZE = 3145728

    class Meta:
        abstract = True

    category = models.ForeignKey(Category, verbose_name='Category', on_delete=models.CASCADE)
    product_name = models.CharField(max_length=200, verbose_name='Product name')
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Product image')
    amount = models.PositiveIntegerField(verbose_name='Amount', default=0)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='price', default=0.0)

    def __str__(self):
        return self.product_name

    def get_model_name(self):
        return self.__class__.__name__.lower()

    # def save(self, *args, **kwargs):
    #     image = self.image
    #     img = Image.open(image)
    #     min_height, min_width = Product.MIN_RESOLUTION
    #     max_height, max_width = Product.MAX_RESOLUTION
    #

    #     if img.height < min_height or img.width < min_width:
    #         raise MinResolErrorExceprion('Image resolution less then minimal ')
    #
    #     if img.height > max_height or img.width > max_width:
    #         raise MaxResolErrorExceprion('Image resolution larger then minimal ')
    # image = self.image
    # img = Image.open(image)
    # new_img = img.convert('RGB')
    # resized_new_img = new_img.resize((400, 200), Image.ANTIALIAS)
    # filestream = BytesIO()
    # resized_new_img.save(filestream,'JPEG', quality=90)
    # filestream.seek(0)
    # name = '{}.{}'.format(*self.image.name.split('.'))
    # self.image = InMemoryUploadedFile(
    #     filestream, 'imageField', name, 'jpeg/image', sys.getsizeof(filestream), None
    # )
    # super().save(*args, **kwargs)


class CartProduct(models.Model):
    user = models.ForeignKey('Customer', verbose_name='Customer', on_delete=models.CASCADE)
    cart = models.ForeignKey('Cart', verbose_name='Cart', on_delete=models.CASCADE, related_name='related_products')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)  # get all models in project
    object_id = models.PositiveIntegerField()  # id of model`s instance
    content_object = GenericForeignKey('content_type', 'object_id')
    qty = models.PositiveIntegerField(default=1)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Summmary price')

    def __str__(self):
        return f'Product : {self.content_object.product_name} (to cart)'

    def save(self, *args, **kwargs):
        self.final_price = self.qty * self.content_object.price
        super().save(*args, **kwargs)


class Cart(models.Model):
    owner = models.ForeignKey('Customer', verbose_name='Owner', on_delete=models.CASCADE, null=True)
    products = models.ManyToManyField(CartProduct, blank=True, related_name='related_cart' )
    total_products = models.PositiveIntegerField(default=0)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Summmary price', default=0)
    in_order = models.BooleanField(default=False)
    for_anonymous_user = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class Customer(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    phone = models.CharField(max_length=20, verbose_name='Phone', blank=True, null=True)
    address = models.CharField(max_length=255, verbose_name='Address', blank=True, null=True)
    orders = models.ManyToManyField(
        'Order', verbose_name='Customer`s orders', related_name='related_customer', null=True, blank=True
    )
    def __str__(self):
        return self.user.username


class Notebook(Product):
    diagonal = models.CharField(max_length=255, verbose_name='Diagonal')
    display_type = models.CharField(max_length=255, verbose_name='Display')
    processor_freq = models.CharField(max_length=255, verbose_name='Processor frequency')
    ram = models.CharField(max_length=255, verbose_name='RAM')
    video = models.CharField(max_length=255, verbose_name='Video')

    def __str__(self):
        return f'{self.category.name} : {self.product_name}'

    def get_absolute_url(self):
        return get_product_url(self, 'shop:product_detail')


class Smartphone(Product):
    diagonal = models.CharField(max_length=255, verbose_name='Diagonal')
    display_type = models.CharField(max_length=255, verbose_name='Display')
    resolution = models.CharField(max_length=255, verbose_name='Resolution')
    accum_volume = models.CharField(max_length=255, verbose_name='Acccum')
    ram = models.CharField(max_length=255, verbose_name='RAM')
    sd = models.BooleanField(default=True)
    camera_mp = models.CharField(max_length=255, verbose_name='Megapixels')

    def __str__(self):
        return f'{self.category.name} : {self.product_name}'

    def get_absolute_url(self):
        return get_product_url(self, 'shop:product_detail')


class Order(models.Model):

    STATUS_NEW = 'new'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_READY = 'is_ready'
    STATUS_COMPLETED = 'completed'

    BUYING_TYPE_SELF ='self'
    BUYING_TYPE_DELIVERY = 'delivery'

    STATUS_CHOICES = (
        (STATUS_NEW, 'New order'),
        (STATUS_IN_PROGRESS, ' Order in progress'),
        (STATUS_READY, ' Order is ready'),
        (STATUS_COMPLETED, 'Order is completed'),
    )

    BUYING_TYPE_CHOICES = (
        (BUYING_TYPE_SELF, 'self-pickup'),
        (BUYING_TYPE_DELIVERY, 'delivery'),
    )

    customer = models.ForeignKey(Customer, verbose_name='Customer', related_name='related_orders',on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, verbose_name='Cart', on_delete=models.CASCADE, null=True, blank=True)
    first_name = models.CharField(max_length=255, verbose_name='First name')
    last_name = models.CharField(max_length=255, verbose_name='Last name')
    phone = models.CharField(max_length=255, verbose_name='Phone')
    address = models.CharField(max_length=1024, verbose_name=' address', null=True, blank=True)
    status = models.CharField(
        max_length=100, choices=STATUS_CHOICES, verbose_name='Status', default=STATUS_NEW
    )
    buying_type = models.CharField(
        max_length=100, choices=BUYING_TYPE_CHOICES, verbose_name='Order type',default=BUYING_TYPE_SELF
    )
    comment = models.TextField(verbose_name='Comment', null=True, blank=True )
    created_at = models.DateTimeField(auto_now=True)
    order_date = models.DateField(verbose_name='Date of receipt',default=timezone.now())

    def __str__(self):
        return str(self.id)