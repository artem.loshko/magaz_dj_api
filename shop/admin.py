from django.contrib import admin
from django.forms import ModelChoiceField, ModelForm, ValidationError

from .models import *




# class NotebookAdminForm(ModelForm):
#
#     # MIN_RESOLUTION = (400, 400)
#     # MAX_RESOLUTION = (2560, 1080)
#
#     def __init__(self,*args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.fields['image'].help_text = mark_safe(
#             '''<span style="color:blue;font-size:14px;">Upload images with min resolution {}x{}</span>
#             '''.format(*Product.MIN_RESOLUTION)
#         )

    # def clean_image(self):
    #     image = self.cleaned_data['image']
    #     img = Image.open(image)
    #     min_height, min_width = Product.MIN_RESOLUTION
    #     max_height, max_width = Product.MAX_RESOLUTION
    #
    #     if image.size > Product.MAX_IMAGE_SIZE:
    #         raise ValidationError('Image size larger then 3 Mb')
    #
    #     if img.height < min_height or img.width < min_width:
    #         raise ValidationError('Image resolution less then minimal ')
    #
    #     if img.height > max_height or img.width > max_width:
    #         raise ValidationError('Image resolution larger then minimal ')
    #
    #     return image

class NotebookAdmin(admin.ModelAdmin):

    # form = NotebookAdminForm
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='notebook'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class SmartphoneAdmin(admin.ModelAdmin):

    change_form_template = 'admin.html'


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='smartphone'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(Notebook, NotebookAdmin)
admin.site.register(Smartphone, SmartphoneAdmin)
admin.site.register(Cart)
admin.site.register(CartProduct)
admin.site.register(Customer)
admin.site.register(Order)
