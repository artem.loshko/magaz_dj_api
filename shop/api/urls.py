from django.urls import path


from .api_views import CategoryListAPIView, SmartphoneListAPIView, SmartphoneDetailAPIView, CustomersListAPIView


app_name = 'api'

urlpatterns = [
    path('categories/', CategoryListAPIView.as_view(), name='categories'),
    path('smartphones/', SmartphoneListAPIView.as_view(), name='smartphones'),
    path('smartphones/<str:id>/', SmartphoneDetailAPIView.as_view(), name='smartphone_detail'),
    path('customers/', CustomersListAPIView.as_view(), name='customers_list')
]
