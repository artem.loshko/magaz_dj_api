from django import template
from django.utils.safestring import mark_safe


register = template.Library()

TABLE_HEAD = """
                <table class="table">
                          <tbody>

"""
TABLE_END = """
                </tbody>
            </table>
"""

TABLE_CONTENT ="""
                <tr>
              <td>{name}</td>
              <td>{value}</td>
            </tr>

"""

PRODUCT_SPEC = {            #{'model_name': {model_fields_name:model_field_value}}
    'notebook': {
        'Diagonal': 'diagonal',
        'Display': 'display_type',
        'Processor frequency': 'processor_freq',
        'RAM': 'ram',
        'Video': 'video',
    },
    'smartphone': {
        'Diagonal': 'diagonal',
        'Display': 'display_type',
        'Resolution': 'resolution',
        'RAM': 'ram',
        'Accum volume': 'accum_volume',
        'SD': 'sd',
        'Camera': 'camera_mp',
    }
}


def get_product_spec(product, model_name):
    table_content = ''
    for name, value in PRODUCT_SPEC[model_name].items():
        table_content += TABLE_CONTENT.format(name=name,
                                              value=getattr(product, value))
    return table_content

@register.filter
def product_spec(product):
    model_name = product.__class__._meta.model_name
    return mark_safe(TABLE_HEAD + get_product_spec(product, model_name) + TABLE_END)
