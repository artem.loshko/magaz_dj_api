from django.urls import path
from .views import index, sign_up, sign_in, logout_, profile_page
app_name = 'users'

urlpatterns = [
    path('', index, name='mainpage'),
    path('sign-up/', sign_up, name='sign_up'),
    path('sign-in/', sign_in, name='sign_in'),
    path('log-out/', logout_, name='logout'),
    path('profile/', profile_page, name='profile' )

]
