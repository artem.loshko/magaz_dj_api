from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms

from .models import UserProfile


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=50, label='First_name', required=False)
    last_name = forms.CharField(max_length=50, label='Last_name', required=False)
    email = forms.EmailField(max_length=100, label='E-mail', required=False)

    def save(self, *args, **kwargs):
        data = self.cleaned_data

        django_user = User.objects.create_user(
            username=data['username'],
            password=data['password1'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            is_active=True
        )
        return django_user


# class UserProfileForm(forms.ModelForm):
#     class Meta:
#         model = UserProfile
#         fields
