from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm
from .models import UserProfile


# Create your views here.

def index(request):
    return render(request, 'index.html', {})

def sign_up(request):
    context = {}
    context['form'] = SignUpForm()

    if request.method == 'POST':
        print('POST method')

        form = SignUpForm(request.POST)
        if form.is_valid():
            django_user = form.save()
            user = authenticate(username=django_user.username,
                                password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('/')
        else:

            context['form'] = SignUpForm(request.POST)
    return render(request, 'users/sign_up.html', context)

def sign_in(request):
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('/')
        else:
            context['error'] = 'Неправильный логин или пароль'
    return render(request, 'users/sign_in.html', context)

@login_required(login_url='users:sign_in')
def logout_(request):
    logout(request)
    return redirect('users:sign_in')

@login_required(login_url='users:sign_in')
def profile_page(request):
    context = {}
    user_profile = UserProfile.objects.get(user=request.user)
    context.update({
        'user': request.user,
        'profile': user_profile
    })
    return render(request, 'users/profile.html', context)
