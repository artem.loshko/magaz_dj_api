from django.contrib import admin
from .models import UserProfile

# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'is_seller']
    list_editable = ['is_seller']


admin.site.register(UserProfile, UserProfileAdmin)